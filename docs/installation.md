<!---
title: Installation
-->

#### Installation
The `sebwite/git` provides a common interface to communiate to both `bitbucket` and `github`  REST API's.

- Add the `sebwite/git` package to your composer.json dependencies.
```json
"require": {
    "sebwite/git": "1.0.*"
}
```

- Register the `GitServiceProvider` in your application, preferably in your `config/app.php` file.
```php
'providers' => [
    Sebwite\Git\GitServiceProvider::class
]
```

- Optional: Add the facade
```php
'facades' => [
    'Asset' => Sebwite\Git\Facades\Git::class
]
```

- Optional: Publish vendor files
```sh
php artisan vendor:publish --provider=Sebwite\Git\GitServiceProvider
```


#### Configuration
The configuration file pretty much speaks for itself. 
You'll want to create a [Github](#) oauth2 token and a [Bitbucket](#) webhook. 
It's recommended you add those settings into your `.env` file like so:
```sh
BITBUCKET_CLIENT_KEY=xxxx
BITBUCKET_CLIENT_SECRET=xxxx
GITHUB_TOKEN=xxxx
```
