<?php


return [
    'transformers' => [
        'github'    => Sebwite\Git\Remotes\Transformers\GithubTransformer::class,
        'bitbucket' => Sebwite\Git\Remotes\Transformers\BitbucketTransformer::class,
    ],
];
