<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Git\Remotes;

/**
 * This is the class TransformerInterface.
 *
 * @package        Sebwite\Git
 * @author         Sebwite
 * @copyright      Copyright () 2015, Sebwite. All rights reserved
 */
interface TransformerInterface
{
    public function transform($data, $functionName);
}
