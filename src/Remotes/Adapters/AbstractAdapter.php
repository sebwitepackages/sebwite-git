<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Git\Remotes\Adapters;

/**
 * This is the class AbstractRemote.
 *
 * @author    Sebwite
 * @copyright Copyright (c) 2015, Sebwite. All rights reserved
 */
abstract class AbstractAdapter
{
    const DRIVER = '';

    /** Instantiates the class
     *
     * @param \Sebwite\Git\Remotes\TransformerInterface $transformer
     * @param array                                     $credentials
     */
    public function __construct()
    {
    }
    public function name()
    {
        return static::DRIVER;
    }

    protected function owner(&$owner)
    {
        return $owner = is_null($owner) ? $this->getUsername() : $owner;
    }

    abstract public function getUsername();
}
