<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Git\Remotes;

use Sebwite\Git\Flysystem\Bitbucket\Settings;
use Sebwite\Git\Remotes\Transformers\NullTransformer;

/**
 * This is the class RemoiteAdapter.
 *
 * @package        Sebwite\Git
 * @author         Sebwite
 * @copyright      Copyright (c) 2015, Sebwite. All rights reserved
 * @mixin          AdapterInterface
 */
class Remote
{
    protected $adapter;

    protected $transformer;

    protected $manager;

    protected $config;

    /** Instantiates the class
     *
     * @param \Sebwite\Git\Contracts\Manager|\Sebwite\Git\Factory $git
     * @param \Sebwite\Git\Remotes\AdapterInterface               $adapter
     * @param \Sebwite\Git\Remotes\TransformerInterface           $transformer
     */
    public function __construct(AdapterInterface $adapter, TransformerInterface $transformer = null)
    {
        $this->adapter     = $adapter;
        $this->transformer = isset($transformer) ? $transformer : new NullTransformer;
    }

    public function __call($name, $arguments)
    {
        if ( method_exists($this->adapter, $name) ) {
            $out = call_user_func_array([ $this->adapter, $name ], $arguments);

            return $this->transformer->transform($out, $name);
        }
    }

    /**
     * connect method
     *
     * @param $config
     *
     * @return $this
     */
    public function connect($config)
    {
        $this->config = $config;

        $this->adapter->connect($this->config);

        return $this;
    }

    /**
     * getFilesystem method
     *
     * @param        $repository
     * @param null   $owner
     * @param string $branch
     * @param string $reference
     *
     * @return \Illuminate\Contracts\Filesystem\Filesystem
     */
    public function getFilesystem($repository, $owner = null, $branch = Settings::BRANCH_MASTER, $reference = Settings::REFERENCE_HEAD)
    {
        $owner = $owner ?: $this->adapter->getUsername();
        $repository = $owner . '/' . $repository;
        $credentials = $this->config;

        $config = compact('repository', 'owner', 'branch', 'reference', 'credentials');
        $config['driver'] = $this->config[ 'driver' ];

        $ckey   = uniqid($this->config[ 'driver' ], false);
        app('config')->set('filesystems.disks.' . $ckey, $config);
        return app('filesystem')->drive($ckey);
    }

    /**
     * Set the transformer value
     *
     * @param \Sebwite\Git\Remotes\Adapters\NullTransformer|\Sebwite\Git\Remotes\TransformerInterface $transformer
     *
     * @return Remote
     */
    public function setTransformer($transformer)
    {
        if ( !$transformer instanceof TransformerInterface ) {
            $transformer = new $transformer;
        }
        $this->transformer = $transformer;

        return $this;
    }

    public function disableTransformer()
    {
        $this->transformer = new NullTransformer;
    }
}
