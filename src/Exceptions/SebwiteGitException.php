<?php
namespace Sebwite\Git\Exceptions;

use RuntimeException;

class SebwiteGitException extends RuntimeException
{
    public static function credentialTypeNotSupported($msg = '')
    {
        return new static('[Credential Type Not Supported] ' . $msg);
    }
}