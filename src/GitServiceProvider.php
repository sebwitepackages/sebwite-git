<?php

namespace Sebwite\Git;

use Github\Client;
use Illuminate\Contracts\Foundation\Application;
use League\Flysystem\Filesystem;
use Sebwite\Git\Remotes\Adapters\Bitbucket;
use Sebwite\Git\Remotes\Adapters\Github;
use Sebwite\Support\ServiceProvider;

/**
 * The main service provider
 *
 * @author        Sebwite
 * @copyright     Copyright (c) 2015, Sebwite
 * @license       https://tldrlegal.com/license/mit-license MIT
 * @package       Sebwite\Git
 */
class GitServiceProvider extends ServiceProvider
{
    protected $dir = __DIR__;

    protected $configFiles = [ 'sebwite.git' ];

    protected $singletons = [
        'sebwite.git' => Manager::class,
    ];

    protected $aliases = [
        'sebwite.git' => Contracts\Manager::class,
    ];

    public function boot()
    {
        $app = parent::boot();

        $app[ 'sebwite.git' ]->extend('github', function () {

            return new Remotes\Remote(new Github());
        });
        $app[ 'sebwite.git' ]->extend('bitbucket', function () {

            return new Remotes\Remote(new Bitbucket());
        });
    }


    public function register()
    {
        $app = parent::register();
        $this->registerBitbucketFS();
        $this->registerGithubFS();

        $app->singleton('sebwite.git.remote', function (Application $app) {
            return $app[ 'sebwite.git' ]->driver();
        });
    }


    protected function registerBitbucketFS()
    {
        $fsm = $this->app->make('filesystem');
        $fsm->extend('bitbucket', function (Application $app, $config) {


            $settings = new \Sebwite\Git\Flysystem\Bitbucket\Settings($config[ 'repository' ], $config[ 'credentials' ], $config[ 'branch' ], $config[ 'reference' ]);
            $api      = new \Sebwite\Git\Flysystem\Bitbucket\Api(new \Bitbucket\API\Api(), $settings);
            $adapter  = new \Sebwite\Git\Flysystem\Bitbucket\BitbucketAdapter($api);

            return new Filesystem($adapter);
        });


        $fsm = $this->app->make('filesystem');
        $fsm->extend('bitbucket2', function (Application $app, $config) {
            $remote = $app->make('sebwite.git')->connection($config[ 'connection' ]);

            $adapter = new \Sebwite\Git\Flysystem\Bitbucket\BitbucketAdapter($remote, collect($config));

            return new Filesystem($adapter);
        });
    }

    protected function registerGithubFS()
    {
        $fsm = $this->app->make('filesystem');
        $fsm->extend('github', function (Application $app, $config) {

            $settings = new \Sebwite\Git\Flysystem\Github\Settings($config[ 'repository' ], $config[ 'credentials' ], $config[ 'branch' ], $config[ 'reference' ]);
            $api      = new \Sebwite\Git\Flysystem\Github\Api(new Client(), $settings);
            $adapter  = new \Sebwite\Git\Flysystem\Github\GithubAdapter($api);

            return new Filesystem($adapter);
        });
    }
}
