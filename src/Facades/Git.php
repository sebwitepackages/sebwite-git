<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Git\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * This is the class Git.
 *
 * @author    Sebwite
 * @copyright Copyright (c) 2015, Sebwite. All rights reserved
 */
class Git extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'sebwite.git';
    }
}
